import java.util.Scanner;

/*
1. Минимальный элемент
На вход передается N — количество столбцов в двумерном массиве и M — количество строк.
Затем сам передается двумерный массив, состоящий из натуральных чисел.

Необходимо сохранить в одномерном массиве и вывести на экран минимальный элемент каждой строки.

Ограничение:
0 < N < 100
0 < M < 100
0 < ai < 1000

Примечание:
Решение должно находиться в файле с названием Solution.java.
Публичный класс с решением должен называться Solution.
Использовать package нельзя.

Пример входных данных
3 2
10 20 15
7 5 9

3 2
10 20
15 7
5 9

Пример выхо
10 5
 */
public class Task01 {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        int m = console.nextInt();
        int n = console.nextInt();


        int[][] arr = new int[n][m];
//        int[][] arr = {{10, 20, 15}, {7, 5, 9}};

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                arr[i][j] = console.nextInt();
            }
        }

        int min = 0;

        int[] arrRes = new int[arr.length];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                if(arr[i][j] < arr[i][min]){
                    min = j;
                }
                arrRes[i] = arr[i][min];
            }
            System.out.print(arrRes[i] + " ");
        }

    }
}
