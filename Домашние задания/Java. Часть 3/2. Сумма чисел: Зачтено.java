import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int m = console.nextInt();
        int n = console.nextInt();
        int res = 0;
        for (int i = m; i <= n; i++) {
            res += i;
        }
        System.out.println(res);
    }
}