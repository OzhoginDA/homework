import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        for (int i = 0; i < n; i++) {
            for (int j = i; j < n - 1; j++) {
                System.out.print(" ");

            }
            for (int k = 0; k < i * 2 + 1; k++) {
                System.out.print("#");
            }
            System.out.println();
        }
        for (int l = 0; l < n - 1; l++) {
            System.out.print(" ");
        }
        System.out.print("|");
    }
}