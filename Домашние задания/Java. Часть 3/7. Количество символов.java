import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        String str = console.nextLine();
        String str2 = str.replaceAll("\\s", "");

        int res = 0;
        for (int i = 0; i < str2.length(); i++) {
            res++;
        }
        System.out.println(res);
    }
}