import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        int p = console.nextInt();
        int m = 0;
        for (int i = 0; i < n; i++) {
            int a = console.nextInt();
            if (a > p) {
                m += a;
            }
        }
        System.out.println(m);
    }
}