import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int n = console.nextInt();
        for (int i = 8; i > 0; i = i / 2) {
            int m = n / i;
            n = n - (m * i);
            System.out.print(m + " ");
        }
    }
}