import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        String[] morse = new String[]{".-", "-...", ".--", "--.", "-..", ".", "...-", "--..", "..", ".---", "-.-",
                ".-..", "--", "-.", "---", ".--.", ".-.", "...", "-", "..-", "..-.", "....",
                "-.-.", "---.", "----", "--.-", "--.--", "-.--", "-..-", "..-..", "..--", ".-.-"};

        char[] russians = {'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М',
                'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'};
        StringBuilder translate1 = russianToMorse(morse, russians);
        System.out.println(translate1);
    }

    private static StringBuilder russianToMorse(String[] morse, char[] russians) {
        String userText = sc.nextLine();
        char[] userTextToCharArray = userText.toCharArray();
        StringBuilder translate = new StringBuilder();
        for (char c : userTextToCharArray) {
            for (int j = 0; j < russians.length; j++) {
                if (russians[j] == c) {
                    translate.append(morse[j]).append(" ");
                }
            }
        }
        return translate;
    }

}
