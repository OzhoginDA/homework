import java.util.Random;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {


        System.out.println("\n-= Это генератор паролей =-\n");
        System.out.println("Введите желаемую длину пароля:");

        passGen();
    }

    private static void passGen() {

        int passLength;

        while (true) {
            passLength = sc.nextInt();

            if (passLength < 8) {
                symbols(passLength);
            } else {
                break;
            }
        }

        String chars = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%&*()_+=|<>?{}[\\]~-";

        for (int i = 0; i < passLength; i++) {
            Random randPwd = new Random();
            char c = chars.charAt(randPwd.nextInt(chars.length()));
            System.out.print(c);
        }
    }
    public static void symbols(int symbol) {
        if ((symbol % 10 == 1) && (symbol % 100 != 11)) {
            System.out.println("Пароль с количеством в " + symbol + " символ небезопасен");
        } else if ((symbol % 10 >= 2) && (symbol % 10 <= 4)
                && !(symbol % 100 == 12 || symbol % 100 == 13 || symbol % 100 == 14)) {
            System.out.println("Пароль с количеством в " + symbol + " символа небезопасен");
        } else {
            System.out.println("Пароль с количеством в " + symbol + " символов небезопасен");
        }
    }
}