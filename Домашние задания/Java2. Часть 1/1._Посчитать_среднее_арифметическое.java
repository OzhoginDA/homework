import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int arrLength = sc.nextInt();
        double [] arr = new double[arrLength];

        double average = 0;
        if (arr.length > 0) {
            double sum = 0;
            for (int j = 0; j < arr.length; j++) {
                arr[j] = sc.nextDouble();
                sum += arr[j];
            }
            average = sum / arr.length;
        }
        System.out.print(average);
    }
}
