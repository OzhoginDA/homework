import java.util.Random;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        theGame();
    }

    private static void theGame() {
        int userNum = 0;
        System.out.println("\n-= Отгадай число загаданное компьютером =-\n");

        while (userNum >= 0) {
            System.out.print("Введите свое число: ");
            userNum = sc.nextInt();
            var pcNumber = new Random().nextInt(10);


            String result;
            if (userNum < 0) {
                System.out.println("Game Over!");
                break;
            }
            if (pcNumber == userNum) {
                System.out.println("Победа!");
                break;
            } else if (pcNumber > userNum) {
                result = "Это число меньше загаданного.\n";
            } else {
                result = "Это число больше загаданного.\n";
            }
            System.out.println(result);
        }
    }
}