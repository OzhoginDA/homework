import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int[] array = getArray();
        var shift = sc.nextInt();

        shiftArray(array, shift);
    }

    public static void shiftArray(int[] array, int shift) {
        int[] temp = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            temp[(i + shift) % array.length] = array[i];
        }

        for (int value : temp)
            System.out.print(value + " ");
        System.out.println();
    }

    public static int[] getArray() {
        int[] array = new int[sc.nextInt()];

        if (array.length > 0) {
            for (int j = 0; j < array.length; j++) {
                array[j] = sc.nextInt();
            }
        }

        return array;
    }
}