import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int[] arrayOne = getArray();
        int[] arrayTwo = getArray();

        System.out.println(Arrays.equals(arrayOne, arrayTwo));
    }

    public static int[] getArray() {
        int[] array = new int[sc.nextInt()];

        if (array.length > 0) {
            for (int j = 0; j < array.length; j++) {
                array[j] = sc.nextInt();
            }
        }

        return array;
    }
}