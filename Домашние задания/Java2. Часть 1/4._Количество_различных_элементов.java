import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = getIntArray();

        getNumbersOfUniqueValues(array);
    }

    private static void getNumbersOfUniqueValues(int[] array) {
        int[] unique = new int[array.length];
        int[] counts = new int[array.length];
        int count = 0;

        for (int i = 0; i < array.length; i++) {
            int element = array[i];
            boolean exists = false;

            for (int j = 0; j < count; j++) {
                if (unique[j] == element) {
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                int number = 1;
                for (int j = i + 1; j < array.length; j++) {
                    if (element == array[j]) {
                        number++;
                    }
                }
                unique[count] = element;
                counts[count] = number;
                count++;
            }
        }

        for (int i = 0; i < count; i++) {
            System.out.println(counts[i] + " " + unique[i]);
        }
    }

    public static int[] getIntArray() {
        System.out.println("Введите длину массива и заполните его: ");
        int[] array = new int[sc.nextInt()];

        if (array.length > 0) {
            for (int j = 0; j < array.length; j++) {
                array[j] = sc.nextInt();
            }
        }

        Arrays.sort(array);

        return array;
    }
}