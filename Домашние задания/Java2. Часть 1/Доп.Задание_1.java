import java.util.Random;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {


        System.out.println("\n-= Это генератор паролей =-\n");
        System.out.println("Введите желаемую длину пароля:");

        passGen();
    }

    private static void passGen() {

        int passLength;

        while (true) {
            passLength = sc.nextInt();

            if (passLength < 8) {
                symbols(passLength);
            } else {
                break;
            }
        }

        StringBuilder randPass = new StringBuilder();

        for (int i = 0; i < passLength; i++) {
            randPass.append(randomCharacter());
        }

        System.out.println(randPass);
    }

    public static char randomCharacter() {

        // '!' - '/' -> 33-47   Specials chars pack.1
        // '0' - '9' -> 48-57   numbers in ASCII
        // ':' - '@' -> 58-64   Specials char pack.2
        // 'A' - 'Z' -> 65-90   upperCase ASCII
        // '[' - '`' -> 91-96   Specials chars pack.3
        // 'a' - 'z' -> 97-122  lowerCase in ASCII
        // '{' - '~' -> 123-126 Specials chars pack.4

        int randomInt = (int) (Math.random() * 93);

        if (randomInt <= 14) { //Specials chars pack.1
            //rand 0-14 -> 33-47 ASCII
            int ascii = randomInt + 33; // 33 - 0 = 33
            return (char) (ascii);

        } else if (randomInt <= 24) { //numbers in ASCII
            // rand 15-24 -> 48-57 ASCII
            int ascii = randomInt + 33; // 48 - 15 = 33
            return (char) (ascii);

        } else if (randomInt <= 31) { //Specials char pack.2
            // rand 25-31 -> 58-64 ASCII
            int ascii = randomInt + 33; //58 - 25 = 33
            return (char) (ascii);

        } else if (randomInt <= 57) { //upperCase ASCII
            // rand 32-57 -> 65-90 ASCII
            int ascii = randomInt + 33; // 65 - 32 = 33
            return (char) (ascii);

        } else if (randomInt <= 63) { //Specials chars pack.3
            // rand 58-63 -> 91-96 ASCII
            int ascii = randomInt + 33; // 91 - 58 = 33
            return (char) (ascii);

        } else if (randomInt <= 89) { //lowerCase in ASCII
            // rand 64-89 -> 97-122 ASCII
            int ascii = randomInt + 33; // 97 - 64 = 33
            return (char) (ascii);

        } else {                      // Specials chars pack.4    //  if (randomInt <= 93)
            // rand 90-93 -> 123-126 ASCII
            int ascii = randomInt + 33; // 123 - 90 = 33
            return (char) (ascii);
        }
    }

    public static void symbols(int symbol) {
        if ((symbol % 10 == 1) && (symbol % 100 != 11)) {
            System.out.println("Пароль с количеством в " + symbol + " символ небезопасен");
        } else if ((symbol % 10 >= 2) && (symbol % 10 <= 4)
                && !(symbol % 100 == 12 || symbol % 100 == 13 || symbol % 100 == 14)) {
            System.out.println("Пароль с количеством в " + symbol + " символа небезопасен");
        } else {
            System.out.println("Пароль с количеством в " + symbol + " символов небезопасен");
        }
    }
}