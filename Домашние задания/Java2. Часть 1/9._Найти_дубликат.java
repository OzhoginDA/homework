import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        String[] array = getStringArray(); //получение отсортированной строки

        getDuplicate(array); //проверка на дубликаты
    }

    private static void getDuplicate(String[] array) {
        String maxWord = ""; //переменная для записи повторяющегося слова
        String word = ""; //временный паттерн сравнения
        int maxCount = 0; // переменная для максимального числа повторов
        int count = 1; // темп переменная подсчета числа повторов

        for (String str : array) { //проходим по строке, чтоб посчитать повторы
            if (str.equals(word)) { //сравнение предыдущего "индекса" со следующим
                count++; //если совпало, то плюсуем
            } else {
                if (count > maxCount) { //если совпало, то записываем в переменную повторяющегося слова
                    maxCount = count; // переписываем переменную максимального числа повторов
                    maxWord = word; // перезапись повторяющегося слова
                }
                word = str; //перезапись паттерна
                count = 1; // подсчет повторов каждого "индекса"
            }
        }

        if (count > maxCount) {
            maxCount = count; //количество повторов
            maxWord = word; //повторяющееся слово
        }

        System.out.println(maxWord);
    }

    public static String[] getStringArray() {
        String[] array = new String[sc.nextInt()]; // создание массива строк

        if (array.length > 0) {
            for (int i = 0; i < array.length; i++) { //инициализация по входящей длине
                array[i] = sc.next().toLowerCase();
            }
        }

        Arrays.sort(array);

        return array;
    }
}