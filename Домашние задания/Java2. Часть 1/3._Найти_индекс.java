import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] array = getIntArray();
        int xElement = sc.nextInt();


        System.out.println(addElement(array, xElement));
    }

    private static int addElement(int[] myArray, int xElement) {

        int[] array = new int[myArray.length + 1];
        System.arraycopy(myArray, 0, array, 0, myArray.length);
        array[myArray.length] = xElement;

        Arrays.sort(array);

        int result = -1;

        int left = 0;
        int right = array.length - 1;

        while (left <= right) {
            int mid = (left + right) / 2;
            if (xElement == array[mid]) {
                result = mid;
                left = mid + 1;
            } else if (xElement < array[mid]) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }

        return result;
    }

    public static int[] getIntArray() {
        int[] array = new int[sc.nextInt()];

        if (array.length > 0) {
            for (int j = 0; j < array.length; j++) {
                array[j] = sc.nextInt();
            }
        }

        Arrays.sort(array);

        return array;
    }
}