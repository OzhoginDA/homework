import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int[] array = getSortedIntArray();

        powArray(array);
    }

    private static void powArray(int[] array) {
        int[] powArray = new int[array.length];

        for (int i = 0; i < array.length; i++) {
            powArray[i] = (int)Math.pow(array[i], 2);
        }

        Arrays.sort(powArray);

        for (int j : powArray) {
            System.out.print(j + " ");
        }
    }

    public static int[] getSortedIntArray() {
        int[] array = new int[sc.nextInt()];

        if (array.length > 0) {
            for (int j = 0; j < array.length; j++) {
                array[j] = sc.nextInt();
            }
        }

        Arrays.sort(array);

        return array;
    }
}