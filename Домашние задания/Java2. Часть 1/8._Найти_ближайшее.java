import java.util.Arrays;
import java.util.Scanner;

public class Solution {
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        int[] array = getSortedIntArray();
        int M = sc.nextInt();

        findMaxNumber(array, M);
    }

    private static void findMaxNumber(int[] array, int M) {

        int tmpLast = 0;
        int tmpNext = 0;

        for (int i : array) {
            if (i == M - 1) {
                tmpLast = i;
            } else if (i == M + 1){
                tmpNext = i;
            }
        }

        if (tmpLast < M && M < tmpNext) {
            System.out.println(M + 1);
        } else {
            System.out.println(M - 1);
        }
    }

    public static int[] getSortedIntArray() {
        int[] array = new int[sc.nextInt()];

        if (array.length > 0) {
            for (int j = 0; j < array.length; j++) {
                array[j] = sc.nextInt();
            }
        }

        Arrays.sort(array);

        return array;
    }
}