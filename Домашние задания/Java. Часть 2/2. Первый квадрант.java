import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int x, y;
        x = console.nextInt();
        y = console.nextInt();
        boolean even = x > 0 && y > 0;
        System.out.println(even);
    }
}