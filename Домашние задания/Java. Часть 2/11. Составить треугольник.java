import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System. in);

        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();

        if (a >= b && a>=c && a<b+c){
            System. out.println(true);

        } else if (b >= a && b >= c && b<a+c){
            System. out.println(true);

        } else if (c >= a && c >= b && c < a+ b){
            System. out.println(true);

        } else {
            System.out.println(false);
        }
    }
}