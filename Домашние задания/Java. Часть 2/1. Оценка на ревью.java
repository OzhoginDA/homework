import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int a, b, c;
        a = console.nextInt();
        b = console.nextInt();
        c = console.nextInt();
        if (a > b && b > c) {
            System.out.println("Петя, пора трудиться");
        } else {
            System.out.println("Петя молодец!");
        }
    }
}