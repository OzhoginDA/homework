import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        double n = console.nextDouble();
        if (Math.log(Math.pow(Math.E, n)) == n) {
            System.out.println(true);
        }
    }
}