import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int a, b, c;
        double d;
        a = console.nextInt();
        b = console.nextInt();
        c = console.nextInt();
        d = Math.pow(b, 2) - 4 * a * c;
        if (d < 0) {
            System.out.println("Решения нет");
        } else {
            System.out.println("Решение есть");
        }
    }
}