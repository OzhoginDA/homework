import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        int x = console.nextInt();
        if ((int) (Math.pow(Math.sin(x), 2) + Math.pow(Math.cos(x), 2)) - 1 == 0) {
            System.out.println(true);
        }
    }
}