import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String hello = console.nextLine();

        int index1 = hello.indexOf('H');
        int index2 = hello.indexOf(' ');

        System.out.println(hello.substring(index1,index2));
        System.out.println(hello.substring(index2+1));
    }
}