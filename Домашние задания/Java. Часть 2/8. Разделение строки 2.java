import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String hello = console.nextLine();

        int index1 = hello.lastIndexOf(' ');
        System.out.println(hello.substring(0, index1));
        System.out.println(hello.substring(index1 + 1));
    }
}