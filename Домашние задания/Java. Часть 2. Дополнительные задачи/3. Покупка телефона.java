import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String phone = console.nextLine();
        int price = console.nextInt();
        if (phone.contains("iphone") || phone.contains("samsung")) {
            if (price >= 50000 && price <= 120000) {
                System.out.println("Можно купить");
            } else {
                System.out.println("Не подходит");
            }
        } else {
            System.out.println("Не подходит");
        }
    }
}