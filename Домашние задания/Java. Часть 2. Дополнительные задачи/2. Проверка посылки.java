import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);
        String str = console.nextLine();
        if (str.length() == 0) {
            System.out.println("все ок");
        } else if (str.contains("камни") && !str.contains("запрещенная продукция")) {
            System.out.println("камни в посылке");
        } else if (str.contains("камни") && str.contains("запрещенная продукция")) {
            System.out.println("в посылке камни и запрещенная продукция");
        } else if (!str.contains("камни") && str.contains("запрещенная продукция")) {
            System.out.println("в посылке запрещенная продукция");
        } else {
            System.out.println("все ок");
        }
    }
}