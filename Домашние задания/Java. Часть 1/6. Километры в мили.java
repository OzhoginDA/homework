import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final double MILES_PER_KILOMETER = 1 / 1.60934, KILOMETER;
        Scanner sc = new Scanner(System.in);
        KILOMETER = sc.nextDouble();
        System.out.println(KILOMETER * MILES_PER_KILOMETER);
    }
}