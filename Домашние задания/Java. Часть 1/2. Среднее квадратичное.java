
import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x1 = sc.nextInt();
        double x2 = sc.nextInt();
        double s = Math.sqrt((x1 * x1 + x2 * x2) / 2);
        System.out.println(s);
    }
}