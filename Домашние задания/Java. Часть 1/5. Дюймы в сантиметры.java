import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final double INCH_PER_CENTIMETER = 2.54, INCH;
        Scanner sc = new Scanner(System.in);
        INCH = sc.nextInt();
        System.out.println(INCH*INCH_PER_CENTIMETER);
    }
}