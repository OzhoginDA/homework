import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final int SECONDS_PER_MINUTE = 60, MINUTES_PER_HOUR = 60, HOURS_PER_DAY = 24;
        long totalSeconds, totalMinutes, currentMinute, totalHours, currentHour;
        Scanner input = new Scanner(System.in);
        totalSeconds = input.nextLong();
        totalMinutes = totalSeconds / SECONDS_PER_MINUTE;
        currentMinute = totalMinutes % MINUTES_PER_HOUR;
        totalHours = totalMinutes / MINUTES_PER_HOUR;
        currentHour = totalHours % HOURS_PER_DAY;

        System.out.println(currentHour + " " + currentMinute);
    }
}