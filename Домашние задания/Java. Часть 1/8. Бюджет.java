import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final double x, y = 30;
        Scanner sc = new Scanner(System.in);
        x = sc.nextDouble();

        System.out.println(x / y);
    }
}