import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final int n, a, b;
        Scanner sc = new Scanner(System.in);
        n = sc.nextInt();
        b = n % 10;
        a = (n - b) / 10;

        System.out.println(b +""+ a);
    }
}