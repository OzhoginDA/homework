import java.util.Scanner;

public class Solution {
    public static void main(String[] args) {
        final int x, y;
        Scanner sc = new Scanner(System.in);
        x = sc.nextInt();
        y = sc.nextInt();
        System.out.println(x / y);
    }
}