
public class Task05 {
    private byte number;
    private String day;


    public DayOfWeek(byte number, String day) {
        this.number = number;
        this.day = day;
    }

    public byte getNumber() {
        return number;
    }

    public void setNumber(byte number) {
        this.number = number;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}